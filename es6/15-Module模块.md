# Module 模块

模块功能主要由两个命令构成：export 和 import。

`export` 用于规定模块的对外接口，`import `用于输入其他模块提供的功能。

## 1.export 命令

```js
//例如从user.js输出变量
var name = "Winkyu";
var age = 18;

export {name,age};

//引入函数
function f1(){}
function f2(){}

export {
    f1 as fn,
    f2
};
```

使用 `as`关键字，可以重命名引入的函数。

> export 命令规定的是对外的接口，必须与模块内部的变量建立一一对应关系。

```js
//写法1
export var m = 1;

//写法2
var m = 1;
export {m};

//写法3
var n = 1;
export {n as m};
```

上面三种写法都是正确的，规定了对外的接口m。其他脚本可以通过这个接口，取到值1。

## 2.import 命令

- 使用 export 命令定义了模块的对外接口以后，其他 JS 文件就可以通过 import 命令加载这个模块。**import命令输入的变量都是只读的。**

```js
import {name,age} from "./user.js";

//重新取名字，使用 as
import {name as myName,age} from "./user.js";
```

- import 后面可以跟 `from` 用于指定模块文件的位置。如果不带路径，只是一个模块名，那么必须有相关配置文件。

```js
myMethod();
import { myMethod } from 'util';
```

> import 命令具有提升效果，会提升到整个模块的头部，首先执行。

- 由于 import 是静态执行，所以不能使用表达式和变量。

```js
// 报错
import { 'f' + 'oo' } from 'my_module';

// 报错
let module = 'my_module';
import { foo } from module;

// 报错
if (x === 1) {
  import { foo } from 'module1';
} else {
  import { foo } from 'module2';
}
```

## 3.模块的整体加载

可以使用星号 `*`指定一个对象，所有输出值都加载在这个对象上。

```js
// circle.js

export function area(radius) {
  return Math.PI * radius * radius;
}

export function circumference(radius) {
  return 2 * Math.PI * radius;
}

//main.js加载上述模块
//方法1
import { area, circumference } from './circle';

//方法2
import * as circle from './circle';
```

## 4.export default

`export default`命令，为模块指定默认输出。一个模块只能有一个默认输出，因此 export default 命令只能使用一次。

```js
// export-default.js
export default function () {
  console.log('foo');
}

//import 可以为该匿名函数指定任意名字。
import customName from './export-default';
```

使用 **export defalut** 时，对应的 import 语句**不需要**大括号，使用 **export** 时，对应的 import 语句**需要**大括号。

```js
// 第一组
export default function fn() { // 输出

}
import fn from 'fn'; // 输入

// 第二组
export function fn() { // 输出

};
import {fn} from 'fn'; // 输入
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)

