# Class 类

## 1.定义类

类是用于创建对象的模板。他们用代码封装数据以处理改数据。类语法由 *类表达式* 和 *类声明* 组成。

### 类声明

要声明一个类，可以使用 class 关键字加类名的形式。

```js
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}
```

> *函数声明* 和 *类声明* 的区别在于，函数声明会提升，类声明不会。

```js
let rt = new Rectangle();
class Rectangle{

}
//ReferenceError: Cannot access 'Rectangle' before initialization
```

### 类表达式

*类表达式*可以命名也可以不命名，命名表达式的名称是该类体的局部名称。可以通过类名（name属性）检索。

```js
let rt1 = class Rectangle1{

}
console.log(rt1.name);//Rectangle1

let rt2 = class Rectangle2{

}
console.log(rt2.name);//Rectangle2
```

## 2.类体和方法定义

一个类的类体是一对大括号 `{}` 中的部分。这是定义类成员的位置，如方法或构造函数。

### 构造函数

`constructor`方法是一个特殊的方法，这种方法用于创建和初始化一个由 class 创建的对象。一个类只能拥有一个名为 “constructor”的特殊方法。如果类包含多个 constructor 的方法，则将抛出 一个 SyntaxError。

一个构造函数可以使用 super 关键字调用一个父类的构造函数。

### 原型方法

```js
class Rectangle{
    constructor(height,width){
        this.height = height;
        this.width = width;
    }
    
    get area(){
        return this.height * this.width;
    }
}
```

### 静态方法

`static`关键字用来定义一个类的静态方法。调用静态方法不需要实例化该类。可以通过类名调用。

```js
class Point{
    constructor(h,w){
        this.h = h;
        this.w = w;
    }
    static name = "Point";
}

const p = new Point(10,5);
console.log(p.name);//undefined
console.log(Point.name);//"Point"
```

### 实例属性

实例的属性必须定义在类的方法里。

```js
class Point{
    constructor(h,w){
        this.h = h;
        this.w = w;
    }
    static name = "Point";
}

const p = new Point(10,5);
console.log(p.name);//undefined
console.log(Point.name);//"Point"
```

静态的或原型的数据属性必须定义在类定义的外部。

```js
Rectangle.staticWidth = 5;
Rectangle.prototype.prototypeWidth = 10;
```

## 3.使用 extends 扩展子类

`extends` 关键字在 *类声明* 或 *类表达式* 中用于创建一个类作为另一个类的一个子类。

```js
class Animal{
    constructor(name){
        this.name = name;
    }

    eat(){
        console.log(`${this.name} 吃饱了`);
    }
}

class Dog extends Animal{
    constructor(name){
        super(name);
    }
    eat(){
        console.log(`${this.name} 没吃饱`);
    }
}

const d = new Dog("哈士奇");
d.eat();//"哈士奇 没吃饱"
```

如果子类中定义了构造函数，那么它必须先调用 `super()` 才能使用 `this` 。

### super 关键字

super 既可以当作函数使用，也可以当作对象使用。

（1）super 作为函数调用时，代表父类的构造函数。

```js
class A{
    constructor(){
        console.log(new.target.name);
    }
}

class B extends A{
    constructor(){
        super();
    }
}

new A();//A
new B();//B
```

上面代码中，new.target 指向当前正在执行的函数。可以看到，在 super() 执行时，它指向的是子类 B 的构造函数，而不是父类 A 的构造函数。也就是说，super() 内部的 this 指向的是 B。

（2）super 作为对象时，在普通方法中，指向父类的原型对象；在静态方法中，执行父类。

```js
class A {
  p() {
    return 2;
  }
}

class B extends A {
  constructor() {
    super();
    console.log(super.p()); // 2
  }
}

let b = new B();
```

上面代码中，子类 B 当中的 super.p()，就是将 super 当作一个对象使用。这时，super 在普通方法之中，指向A.prototype，所以 super.p() 就相当于 A.prototype.p()。

```js
class Parent {
  static myMethod(msg) {
    console.log("static");
  }

  myMethod(msg) {
    console.log("instance");
  }
}

class Child extends Parent {
  static myMethod() {
    super.myMethod();
  }

  myMethod() {
    super.myMethod();
  }
}

Child.myMethod();   //"static"
new Child().myMethod(); //"instance"
```

上面代码中，super在静态方法之中指向父类，在普通方法之中指向父类的原型对象。

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)

[4] [ 类-JavaScript|MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Classes)
