# Iterator 迭代器

## 1.概念

`Iterator` 迭代器是一种接口，为各种不同的数据结构提供统一的访问机制。

任何数据结构只要部署 Iterator 接口，就可以完成遍历操作。

**Iterator 的作用：**

- 为各种数据结构提供统一的访问接口。
- 使数据结构的成员能够按照某种次序排序。
- ES6 提供新的遍历方式 `for...of`，Iterator 接口主要供 for...of 消费。

**Iterator 遍历过程：**

1. 创建一个指针对象，指向当前数据结构的起始位置。
2. 第一次调用指针对象的 next() 方法，可以将指针指向数据结构的第一个成员。
3. 第二次调用指针对象的 next() 方法，指针就指向数据结构的第二个成员。
4. 不断调用指针对象的  next() 方法，直到它指向数据结构的结束位置。

每一次调用 next() 方法，都会返回一个包含 value 和 done 两个属性的对象。其中，value属性是当前成员的值，done属性是一个布尔值，表示遍历是否结束。

```js
let it = makeIterator(['a', 'b']);
console.log(it.next()); // { value: "a", done: false }
console.log(it.next()); // { value: "b", done: false }
console.log(it.next()); // { value: undefined, done: true }

function makeIterator(array) {
  let nextIndex = 0;
  return {
    next: function() {
      return nextIndex < array.length ?
        {value: array[nextIndex++], done: false} :{value: undefined, done: true};
    }
  };
}
```

## 2.默认 Iterator 接口

默认的 Iterator 接口部署在数据结构的 Symbol.iterator 属性，或者说，一个数据结构只要具有 Symbol.iterator 属性，就认为是可遍历的。

原生具备 Iterator 接口的数据结构有：String、Array、Map、Set、函数的 arguments 对象。

Object 对象没有默认部署 Iterator 接口，是因为没法确定对象属性的遍历顺序。

- 数组的 Iterator 接口

```js
let arr = [1, 2, 3];
let it = arr[Symbol.iterator]();

console.log(it.next());//{value: 1, done: false}
console.log(it.next());//{value: 2, done: false}
console.log(it.next());//{value: 3, done: false}
console.log(it.next());//{value: undefined, done: true}
```

- 字符串的 Iterator 接口

```
let str = "字符串";
let it = str[Symbol.iterator]();

console.log(it.next());  // {value: "字", done: false}
console.log(it.next());  // {value: "符", done: false}
console.log(it.next());  // {value: "串", done: false}
console.log(it.next());  // {value: undefined, done: true}
```

## 3.调用 Iterator 接口的场合

- **解构赋值**

```js
let set = new Set().add('a').add('b').add('c');

let [first, ...rest] = set;
console.log([first]); //["a"]
console.log([rest]);  //["b","c"]
```

- **扩展运算符**

```js
//字符串解构
let str = 'hello';
console.log([...str]); // ["h", "e", "l", "l", "o"]

//数组合并
let arr = ['b', 'c'];
let arr1 = ['a', ...arr, 'd'];
console.log(arr1);// ["a", "b", "c", "d"]
```

由于数组的遍历会调用遍历器接口，所以任何接受数组作为参数的场合，其实都调用了遍历器接口。比如：

for...of、Array.from()、Map()、Set() 等。

## 4.for...of 循环

`for...of`在可迭代对象（包括 Array，String，Map，Set，arguments 对象等等）上创建一个迭代循环，调用自定义迭代钩子，并为每个不同属性的值执行语句。

格式：

```js
for (variable of iterable) {
    
}
//variable：在每次迭代中，将不同属性的值分配给变量。
//iterable：被迭代枚举其属性的对象。
```

- **迭代 Array**

```js
let iterable = [10, 20, 30];

for (const value of iterable) {
  console.log(value);
}
// 10
// 20
// 30
```

- **迭代 String**

```js
let iterable = "字符串";

for (let value of iterable) {
  console.log(value);
}
// "字"
// "符"
// "串"
```

- **迭代 Map**

```js
let iterable = new Map([["a", 1], ["b", 2], ["c", 3]]);

for (let entry of iterable) {
  console.log(entry);
}
// ["a", 1]
// ["b", 2]
// ["c", 3]
```

- **迭代 Set**

```js
let iterable = new Set([1, 2, 3]);

for (let value of iterable) {
  console.log(value);
}
// 1
// 2
// 3
```

- **迭代 arguments 对象**

```js
(function() {
  for (let argument of arguments) {
    console.log(argument);
  }
})(1, 2, 3);
// 1
// 2
// 3
```

与 `for...in`的区别：

for...in 只能获取对象的键名，不能直接获取键值，而 for...of 可以获取键值。

```js
let arr = ['a', 'b'];

for (let a in arr) {
  console.log(a);
}
//0
//1

for (let a of arr) {
  console.log(a);
}
//"a"
//"b"
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)