# Set 和 Map

## 1.Set

`Set` **类似数组，但允许存储任何类型的唯一值**，实现了 iterator 接口，所以可以使用扩展运算符和 for ... of 进行遍历。

### 基本使用

- +0 与 -0 在存储判断唯一性的时候是恒等的，所以不重复；
- undefined 与 undefined 是恒等的，所以不重复；
- NaN 与 NaN 是不恒等的，但是在 Set 中只能存一个，不重复。

```js
let set = new Set();
set.add(+0);
set.add(-0);
set.add(undefined);
set.add(undefined);
set.add(NaN);
set.add(NaN);
console.log(set);//{0, undefined, NaN}

//两个空对象不相等
let set1 = new Set();
set1.add({});
set1.add({});
console.log(set1);     //{{},{}}
console.log(set1.size);//set实例成员的个数：2
```

数组、字符串转为 Set。

```js
//Array 转 Set
let set = new Set([1, 2, 2, 3]);
console.log(set);   //{1, 2, 3}
console.log(set.size);//3，表示去除了重复值

//Set 转 Array
let arr = [...set];
console.log(arr);   // [1, 2, 3]

//String 转 Set
let mySet = new Set("hello");
console.log(mySet); //{"h", "e", "l", "o"}
```

Set 对象还可用作实现集合的并集、交集、差集。

```js
let set1 = new Set([1, 2, 3]);
let set2 = new Set([4, 3, 2]);

//并集
let union = new Set([...set1, ...set2]);
console.log(union);//{1, 2, 3, 4}

//交集
let intersect = new Set([...set1].filter(x => set2.has(x)));
console.log(intersect);//{2, 3}

//差集
let difference = new Set([...set1].filter(x => !set2.has(x)));
console.log(difference);//{1}
```

### 属性和方法

- `size`属性可用于返回 Set 实例的成员个数。

- `add(value)`：添加某个值，返回 Set 结构本身。

- `delete(value)`：删除某个值，返回一个布尔值，表示删除是否成功。

- `has(value)`：返回一个布尔值，表示该值是否为 Set 的成员。

- `clear()`：清除所有成员，没有返回值。

```js
let set = new Set(['大事儿','小事儿','好事儿','坏事儿','小事儿']);

//元素个数
console.log(set.size);//4
//添加新的元素
set.add('喜事儿');
console.log(set);//{"大事儿", "小事儿", "好事儿", "坏事儿", "喜事儿"}
//删除元素
set.delete('坏事儿');
console.log(set);//{"大事儿", "小事儿", "好事儿", "喜事儿"}
//检测
console.log(set.has('糟心事'));//false
//清空
set.clear();
console.log(set);//{}
```

**遍历方法：**

- `keys()` ：返回键名。
- `values()`：返回键值。
- `entries()`：返回键值对。
- `forEach()`：遍历 Set 的所有成员。

Set 的遍历顺序就是元素的插入顺序。由于 Set 没有键名，只有键值，或者说键名和键值一样，所以 keys() 和 values() 方法的使用效果一致。

```js
let set = new Set(["s1","s2","s3"]);
for (const item of set.keys()) {
  console.log(item);
}
//s1
//s2
//s3

for (const item of set.values()) {
  console.log(item);
}
//s1
//s2
//s3

for (const item of set.entries()) {
  console.log(item);
}
//["s1", "s1"]
//["s2", "s2"]
//["s3", "s3"]

for (const item of set) {
  console.log(item);
}
//s1
//s2
//s3
```

## 2.Map

`Map`**类似对象，但是“键”可以是各种类型的值，不限字符串。**Map 也实现了iterator 接口，可以使用扩展运算符和 for ... of 进行遍历。

### 基本使用

```js
let m = new Map();

//key 是字符串
let str = "aaa";
m.set(str,"字符串");
console.log(m.get(str));  //"字符串"
console.log(m.get("aaa"));//"字符串"

//key 是对象
let obj = {};
m.set(obj,"对象");
console.log(m.get(obj));//"对象"
console.log(m.get({})); //undefined,因为 obj 和 {} 不是同一个对象

//key 是函数
let fn = function(){};
m.set(fn,"函数");
console.log(m.get(fn));//"函数"
console.log(m.get(function(){})); //undefined,因为 fn 和 function(){} 不是同一个函数
```

如果 Map 的 key 是简单类型的值（数字、字符串、布尔值），只要两个值严格相等，就将其视为一个键，比如 0 和 -0 就是一个键，布尔值 true 和字符串 ‘true’ 则是两个不同的键。注意，虽然 NaN 不严格相等于自身，但 Map 将其视为同一个键。

### 属性和方法

- `size`属性返回 Map 结构的成员总数。
- `set(key,value)`：设置键名 key 对象的键值 value，返回整个 Map 结构。如果 key 已经有值，则会被覆盖。
- `get(key)`：读取 key 对应的键值，如果找不到 key，返回 undefined。
- `has(key)`：返回一个布尔值，表示某个键是否在当前 Map 对象之中。
- `delete(key)`：删除某个键，返回 true。如果删除失败，返回 false。
- `clear()`：清除所有成员，没有返回值。

```js
let m = new Map();

//设置键值
m.set("id",001);
m.set("name","Winkyu");
m.set(123,456);
console.log(m);// {"id" => 1, "name" => "Winkyu", 123 => 456}

//成员数量
console.log(m.size);//3
//获取键值
console.log(m.get("name"));//"Winkyu"
//判断键是否存在
console.log(m.has(123));//true
//删除键
console.log(m.delete("id"));//true
console.log(m);//{"name" => "Winkyu", 123 => 456}
//清空集合
m.clear();
console.log(m.size);//0
```

**遍历方法：**

- `keys()` ：返回键名。
- `values()`：返回键值。
- `entries()`：返回键值对。
- `forEach()`：遍历 Map 的所有成员。

Map 的遍历顺序就是插入顺序。

```js
let map = new Map([
  [1, "name"],[2, "age"]
]);

for (let key of map.keys()) {
  console.log(key);
}
//1
//2

for (let value of map.values()) {
  console.log(value);
}
//"name"
//"age"

for (let [key,value] of map.entries()) {
  console.log(key,value);
}
//1 "name"
//2 "age"

//等同于map.entries()
for (let [key, value] of map) {
  console.log(key, value);
}
```

### 与其他数据转换

- **数组 与 Map 的互转**

```js
let map = new Map();
map.set(1, "lala").set(2, {person: "obj"});

//Map->Array,使用扩展运算符
let arr = [...map];
console.log(arr);//[[1,"lala"],[2,{"person": "obj"}]]

//Array->Map，直接将数组传入Map构造函数
let m = new Map(arr);
console.log(m);//{1 => "lala",2 => {person: "obj"}}
```

- **对象 与 Map 的互转**

> Map 转 对象 如果所有 Map 的键都是字符串，它可以无损地转为对象。
>
> 如果有非字符串的键名，那么这个键名会被转成字符串，再作为对象的键名。

```js
//Map->Object,自定义遍历赋值
function mapToObject(map) {
  let obj = Object.create(null);
  for (let [k,v] of map) {
    obj[k] = v;
  }
  return obj;
}

let map = new Map().set('yes', true).set('no', false);
console.log(mapToObject(map));// { yes: true, no: false }

//Object->Map，通过Object.entries()
let obj = {"a":1, "b":2};
let m = new Map(Object.entries(obj));
console.log(m);//{"a" => 1, "b" => 2}
```

- **Map 转为 JSON** ，分两种情况：

```js
//1、Map 所有键名都是字符串
let map1 = new Map().set("id", 123).set("age", 18);
let json1 = mapToJson(map1);
console.log(json1);  //{"id":123,"age":18}

function mapToJson (map) {
    let obj = Object.create(null);
    for (let [k,v] of map) {
        obj[k] = v;
    }
    return JSON.stringify(obj);
}

//2、Map 键名有非字符串，使用JSON.stringify()
let map2 = new Map().set(true, 1).set({"id": 123}, [1,2,3]);
let json2 = JSON.stringify([...map2]);
console.log(json2);//[[true,1],[{"id":123},[1,2,3]]]
```

- **JSON 转为 Map** ，分为两种情况：

```js
//1、所有键名都是字符串
let json1 = {"id":123,"age":18};
console.log(jsonToMap(json1));//{"id" => 123, "age" => 18}

function jsonToMap (json) {
  let map = new Map();
  for (let k of Object.keys(json)) {
    map.set(k, json[k]);
  }
  return map;
}

//2、整个 JSON 就是一个数组，且每个数组成员本身，又是一个有两个成员的数组
let json2 = '[[true,1],[{"id":123},[1, 2, 3]]]';
let map2 = new Map(JSON.parse(json2));
console.log(map2);//{true => 1, {id: 123} => [1, 2, 3]}
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)
