# async 函数

## 1.基本用法

`async`函数就是 Generator 函数的[语法糖]()（指计算机语言中添加的某种语法）。

async 函数就是将 Generator 函数的星号（*）替换成 async，将 yeild 替换成 await。

它的实现原理就是将 Generator 函数和自动执行器，包装在一个函数里。

```js
//函数声明
async function fn(){};
//函数表达式
let fn = async function(){};
```

async 函数对比 Generator 函数的改进：

（1）语义化强。

async 和 await，比起星号和 yield，语义更清楚了。async 表示函数里有异步操作，await 表示紧跟在后面的表达式需要等待结果。

（2）async 函数返回值的是一个 Promsie 对象。

比 Generator 函数的返回值是 Iterator 对象方便。可以用 then 方法指定下一步的操作。

```js
function testAwait(){
   return new Promise((resolve) => {
       setTimeout(function(){
          console.log("testAwait");
          resolve();
       }, 1000);
   });
}
 
async function helloAsync(){
   await testAwait();
   console.log("helloAsync");
 }
helloAsync();
```

**await 命令**	

async 函数执行时，遇到 await 就会先暂停执行，等到触发的异步完成后，恢复 async 函数的执行并返回解析值。 

await针对所跟不同表达式的处理方式：

- Promise 对象：await 会暂停执行，等待 Promise 对象 resolve，然后恢复 async 函数的执行并返回解析值。
- 非 Promise 对象：直接返回对应的值。

## 2.注意事项

（1）await 命令后面的 Promise 对象，运行结果可能是 rejected，所以最好把 await 命令放在 try…catch 代码块中。

```js
async function fn(){
    try{
        await new Promise((resolve,reject) => {
            throw new Error("出错了");
        });
    }catch(e){
        console.log(e);
    }
    return await("hello");
}

```

（2）多个 await 命令后面的异步操作，如果不存在继发关系，最好让它们同时触发。

```js
async function fn(){
    try {
        // 写法一
        let [foo, bar] = await Promise.all([getFoo(), getBar()]);

        // 写法二
        let foo1 = await getFoo();
        let bar1s = await getBar();
    } catch (error) {
    }
};
```

（3）await 命令只能用在 async 函数之中，如果用在普通函数，就会报错。

（4）async 函数可以保留运行堆栈。

## 3.与Promise、Generator比较

下面分别三种异步方式来读取文件：a.txt、b.txt、c.txt

```js
const fs = rquire("fs");
const read = function(file){
    return new Promise((resolve,reject) => {
        fs.readFile(file,(err,data) => {
            if(err){
                reject(err);
            }else{
                resolve(data);
            }
        });
    });
};

```

- 用 Promise 读取文件

```js
read("a.txt").then(res => {
    console.log(res.toString());
    return read("b.txt");
}).then(res => {
    console.log(res.toString());
    return read("c.txt");
}).then(res => {
    console.log(res.toString());
});
```

- 用 Generator 函数读取

```js
function * readFile(){
    yield read("a.txt");
    yield read("b.txt");
    yield read("c.txt");
}
const rf = readFile();
rf.next().value.then(res => {
    console.log(res.toString());
    return rf.next().value;
}).then(res => {
    console.log(res.toString());
    return rf.next().value;
}).then(res => {
    console.log(res.toString());
});
```

- 用 async 函数读取

```js
async function readFile(){
    let a = await read("a.txt");
    console.log(a);
    let b = await read("b.txt");
    console.log(b);
    let c = await read("c.txt");
    console.log(c);
}
```

总结：

Promise 是基础，async 函数和 Generator 函数类似，但是 async 更简洁。

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)

[4] [ 深入浅出ES6教程『async函数』](https://www.jianshu.com/p/631f9406c4e0)
