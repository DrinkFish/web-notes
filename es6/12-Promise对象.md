# Promise 对象

## 1.基本使用

`Promise`是一个对象，可以获取异步操作的消息。有两个特点：

- **对象的状态不受外界影响。**有三种状态，`pending`（进行中）、`fufilled`（已成功）、`rejected`（已失败）。只有异步操作的结果，可以决定当前是哪一种状态，任何其他操作都无法改变这个状态。
- **一旦状态改变，任何时候都可以得到这个结果。**它的状态只能从 pending --> fufilled，pending --> rejected。

优点：

- 指定回调函数的方式更加灵活
- 支持链式调用，可以解决毁回调地域的问题



缺点：

- 一旦创建立即执行，无法中途取消。
- 当处于 pending 状态时，无法得知是刚开始还是即将完成。
- 若不设置回调函数，内部会抛出错误，不会反应到外部。

格式：

```js
let promise = new Promise(function(resolve, reject) {
  if (/* 异步操作成功 */){
    resolve(value);
  } else {
    reject(error);
  }
});
```

Promise 构造函数接收一个函数，该函数有两个参数，分别是：

-  `resolve`：异步操作成功调用（pending --> resolved）。

- `reject`：异步操作失败调用（pending --> rejected）。

Promise 的基本流程：

![image-20210813111735275](https://gitee.com/Winkyu/blog-imgs/raw/master/javadoc/20210813111743.png)

**then() 方法**

then 方法接收两个函数作为参数。第一个参数是 Promise 执行成功时的回调，第二个参数是 Promise 执行失败时的回调，**两个函数只会有一个被调用。**在事件运行完成之前，回调函数不会被调用。

then() 方法是异步执行的。

then 方法返回的是一个新的 Promise 实例，可以采用链式写法。

```js
let p = new Promise((resolve,reject) => {
    //异步操作
    setTimeout(() => {
        //生成1-10随机数
        let num = Math.round(Math.random()*10 + 1);
        if(num > 5){
            //成功，调用resolve(value)
            resolve(num);
        }else{
            //失败，调用reject(reason)
            reject(num);
        }
    }, 1000);
});

p.then((value) => {
    console.log(value+"大于5");
},(reason) => {
    console.log(value+"小于5");
});
```

## 2.Promise API

### 2.1 构造函数：Promise(executor){}

- executor 函数：同步执行 (resolve, reject) => {}

- resolve 函数：内部定义成功时调用的函数 resove(value)

- reject 函数：内部定义失败时调用的函数 reject(reason)

  说明：executor 会在 Promise 内部立即同步回调，异步操作 resolve/reject 就在 executor 中执行

### 2. 2 Promise.prototype.then()

p.then(onResolved, onRejected)指定两个回调（成功+失败）

- onResolved 函数：成功的回调函数 (value) => {}

- onRejected 函数：失败的回调函数 (reason) => {}

### 2.3 Promise.prototype.catch()

用于指定发生错误时的回调函数。

- onRejected 函数：失败的回调函数 (reason) => {}

```js
const p = new Promise((resolve, reject) => { // excutor执行器函数
    //模拟异步
    setTimeout(() => {
        let num =  Math.round(Math.random()*10 + 1);
        if(num > 5) {
            resolve(num +"成功的数据");
        } else { 
            reject(num +"失败的数据");
        }
    }, 1000);
}).then(value => {
    //成功，调用 onResolved()函数
    console.log(value);
}).catch(reason => {
    //失败，调用 onRejected()函数
    console.log(reason);
});
```

### 2.4 Promise.resolve(value)

value ：一个成功或失败的 Promise 对象。

- 如果 value 是非 Promise 类型对象，则返回结果为成功 Promise 对象。

- 如果 value 是 Promise 类型对象，则传入参数的结果决定了 resolve 的结果。

```js
let p = Promise.resolve(111);
console.log(p);
//[[PromiseState]]: "fulfilled"
//[[PromiseResult]]: 111

let p2 = Promise.resolve(new Promise((resolve,reject) => {
    // resolve(222);
    // [[PromiseState]]: "fulfilled" 成功的 Promise
    // [[PromiseResult]]: 222
    reject("333");
    // [[PromiseState]]: "rejected" 失败的 Promise
    // [[PromiseResult]]: "333"
}));
console.log(p2);
```

### 2.5 Promise.reject(reason)

返回一个失败的 Promise 对象。

```js
let p1 = Promise.reject(111);
let p2 = Promise.reject(new Promise((resolve,reject)=> {
    resolve("ok");
}));
console.log(p1);
// [[PromiseState]]: "rejected"
// [[PromiseResult]]: 111

console.log(p2);
// [[PromiseState]]: "rejected"
// [[PromiseResult]]: Promise
```

### 2.6 Promise.all(iterable)

返回一个新的 Promise，只要所有的 Promise都成功返回才成功，只要有失败的就返回失败，返回结果就是失败的值。

iterable：可传入多个Promise 的可迭代对象，如数组或字符串。

```js
let p1 = new Promise((resolve,reject) => {
    resolve("ok");
});
let p2 = Promise.reject("error");
let p3 = Promise.resolve("ok");

let np1 = Promise.all([p1,p2,p3]);
console.log(np1);
// [[PromiseState]]: "rejected" 失败的
// [[PromiseResult]]: "error"   p2的值

let np2 = Promise.all([p1,p3]);
console.log(np2);
// [[PromiseState]]: "fulfilled" 成功
// [[PromiseResult]]: Array(2)   值为传入的值
    // 0: "ok"
    // 1: "ok"
    // length: 2
```

### 2.7 Promise.race(iterable)

返回一个新的 Promise，第一个完成的 Promise 的结果状态就是最终的结果状态，不管是成功还是失败。

```js
let p1 = new Promise((resolve, reject) => {
    //延迟执行
    setTimeout(() => {
        resolve('OK');
    }, 2000);
});
let p2 = Promise.reject('Success');

const result = Promise.race([p1, p2]);
console.log(result);
// [[PromiseState]]: "rejected"  失败状态
// [[PromiseResult]]: "Success"  返回结果
```

## 3.关于 Promise 的一些问题

### 3.1 如何改变 Promise 状态？

（1）resolve(value)：如果当前是 pending 就会变为 resolved

（2）reject(reason)：如果当前是 pending 就会变为 rejected

（3）抛出异常：如果当前是 pending 就会变为 rejected

### 3.2 一个 Promise 指定多个成功/失败回调函数，都会调用吗？

当 Promise 改变为对应状态时都会调用。

### 3.3 改变 Promise 状态和指定回调函数谁先谁后？

（1）都有可能，正常情况下是 先指定回调再改变状态，但也可以先改变状态再指定回调。

（2）如何先改状态在指定回调？

- 在执行器中直接调用 resolve()/reject()。

- 延迟更长时间才调用 then()。

```js
let p = new Promise((resolve,reject) => {
    resolve("ok");  //同步调用时
});

p.then(value =>{
    console.log(value);
},reason => {
    console.log(reason);
});
```

（3）什么时候才能得到数据？

- 如果先指定的回调，那当状态发生改变时，回调函数就会调用得到数据。

- 如果先改变的状态，那当指定回调时，回调函数就会调用得到数据。

```js
new Promise((resolve,reject) => {
    resolve("ok");  //同步调用,先改变状态
}).then(value =>{
    console.log(value);
},reason => {
    console.log(reason);
});

new Promise((resolve,reject) => {
    setTimeout(() => {  //异步，先指定回调函数
        resolve("ok");
    }, 1000);
}).then(value =>{
    console.log(value);
},reason => {
    console.log(reason);
});
```

### 3.4 Promise.then() 返回的新 Promise 的结果状态由什么决定？

简单来说，由 then() 方法指定的回调函数执行结果决定。

```js
let p = new Promise((resolve,reject) => {
    resolve("ok");  //成功，then() 方法执行 value =>{}
});

let result = p.then(value =>{
    console.log(value);
},reason => {
    console.log(reason);
});
console.log(result);
// [[PromiseState]]: "fulfilled"    成功
// [[PromiseResult]]: undefined
```

### 3.5 Promise 如何创建多个操作任务？

（1）Promise 的 then() 返回一个新的 Promise，可以合并成 then() 的链式调用。

（2）通过 then() 的链式调用串联多个任务。

```js
let p = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve('OK');
  }, 1000);
});

p.then(value => {
  return new Promise((resolve, reject) => {
      resolve("success");
  });
}).then(value => {
  console.log(value); // success
}).then(value => {
  console.log(value); // undefined
})
```

### 3.6 什么是 Promise 异常穿透？

当使用 Promise 的 then() 链式调用时，可以在最后指定失败的回调，前面任何操作出现了异常，都会传到最后失败的回调中处理。

```js
let p = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve('OK');
  }, 1000);
});

p.then(value => {
    throw "失败了";
}).then(value => {
    console.log(111);
}).then(value => {
    console.log(222);
}).catch(reason => {
    console.log(reason);//这里可以接收到第一个 then()中抛出的异常
});
```

### 3.7 如何中断 Promise 链？

在回调函数中返回一个 pending 状态的 Promise 对象。

```js
let p = new Promise((resolve, reject) => {
  setTimeout(() => {
      resolve('OK');
  }, 1000);
});

p.then(value => {
    console.log(111);
    return new Promise(() => {});   //pending 状态的Promise
}).then(value => {
    console.log(222);
}).catch(reason => {
    console.log(reason);
});
//111   后续console.log(222);不会执行
```

## 4.Promise 的自定义封装

```js
//定义常量
const PENDING  = "pending";
const RESOLVED = "resolved";
const REJECTED = "rejected";

//自定义 Promise 类
class Promise{

    /**
     * Promise 构造函数
     * @param executor 执行器函数 (resolve,reject) => {} 
     */ 
    constructor(executor){
        //保存当前实例对象的 this
        const self = this;
        //添加属性
        self.PromiseState = PENDING;
        self.Promiseresult = null;
        self.callbacks = [];    //存储对象结构：{onResolvedd(){},onRejected(){}}
        
        /**
         * 成功时调用的函数
         * @param value 成功时的值
         */ 
        function resolve(value){
            //如果当前状态不是 pending，直接结束
            if(self.PromiseState !== PENDING) return;
            //修改对象的状态、结果值、回调函数
            self.PromiseState = RESOLVED;
            self.Promiseresult = value;
            if(self.callbacks.length > 0 ){
                //执行队列中所有成功的回调
                self.callbacks.forEach(obj => {
                    obj.onResolved(value);
                });
            }
        }

        /**
         * 失败时调用的函数
         * @param reason 失败的原因
         */ 
        function reject(reason){
           //如果当前状态不是 pending，直接结束
           if(self.PromiseState !== PENDING) return;
            //修改对象的状态、结果值、回调函数
            self.PromiseState = REJECTED;
            self.Promiseresult = reason;
            if(self.callbacks.length > 0 ){
                //执行队列中所有成功的回调
                self.callbacks.forEach(obj => {
                    obj.onRejected(reason);
                });
            }
        }

        //立即执行 executor 函数
        try {
            executor(resolve,reject);
        } catch (error) {
            // 如果执行器抛出异常，Promise 对象变成 rejected 状态
            reject(error);
        }
    }

    /**
     * Promise 原型对象 then 方法
     * @param onResolved 成功的回调函数 (value) => {} 
     * @param onRejected 失败的回到函数 (reason) => {} 
     * @return 一个新的 Promise 对象，结果由执行的回调函数 onResolved、onRejected 决定
     */ 
    then(onResolved,onRejected){
        const self = this;
       // 如果 onResolved/onRejected 不是函数，为它指定一个默认函数
        if(typeof onResolved !== 'function'){
            onResolved = (value) => {return value};
        }
        if(typeof onRejected !== 'function'){
            onRejected = (reason) => {return reason};
        }

        return new Promise((resolve,reject) => {
            /**
             * 调用指定回调函数处理，根据执行的结果改变 Promise 的状态
             * @param callback 指定回调函数
             */
            function handle(callback) {
                try {
                    //result获取回调函数执行(return)的结果
                    const result = callback(self.PromiseResult); 
                    if (result instanceof Promise) {
                        //如果回调函数返回的是 promise
                        result.then(resolve, reject);
                    } else {
                        //如果回调函数返回的不是 promise
                        resolve(result)
                    }
                } catch (error) {
                    //如果抛出异常
                    reject(error);
                }
            }
            
            //判断 Promise 状态
            if(self.PromiseState === PENDING){
                // 当前状态是 pending 状态，将回调函数保存起来
                self.callbacks.push({
                    // 执行成功的回调函数，改变 Promise 的状态
                    onResolved(value){
                        handle(onResolved);
                    },
                    // 执行失败的回调函数，改变 Promise 的状态
                    onRejected(reason){
                        handle(onRejected);
                    }
                });
                // 执行 onResolved,并改变 Promise 的状态
            }else if(self.PromiseState === RESOLVED){
                handle(onResolved);
                // 执行 onRejected,并改变 Promise 的状态
            }else if(self.PromiseState === REJECTED){
                handle(onRejected);
            }
        });
    }

    /**
     * Promise 原型对象 catch 方法
     * @param onRejected 失败的回调函数 (reason) => {}
     * @return 一个新的 Promise 对象
     */ 
    catch(onRejected){
        return this.then(undefined,onRejected);
    }

    /**
     * Promise 函数对象 resolve 方法
     * @param value 成功的值
     * @returns 一个成功/失败的 Promise
     */
    static resolve(value){
        return new Promise((resolve,reject) => {
            // value是 Promise => 使用value的结果作为 Promise 的结果
            if (value instanceof Promise) {
                value.then(resolve, reject);
            } else {
                // value 不是 Promise => Promise 状态变为成功，数据是 value
                resolve(value);
            }
        });
    }

    /**
     * Promise 函数对象 reject 方法
     * @param reason 失败的原因
     * @returns 一个失败的 promise
     */
    static reject(reason){
        return new Promise((resolve,reject) => {
           reject(reason); 
        });
    }

    /**
     * Promise 函数对象 all 方法
     * @param promises 一组 promise
     * @returns 一个 Promise，只有当所有 Promise 都成功时才成功，否则只要有一个失败就失败
     */
    static all(promises){
        return new Promise((resolve,reject) => {
            //声明计数变量
            let count = 0;
            //指定数组长度
            const values = new Array(promises.length);
            for (let i = 0; i < promises.length; i++) {
                //防止数组中有不是 Promise 的元素
                Promise.resolve(promises[i]).then(value => {
                    //若对象的状态是成功
                    count++;
                    //将当前 Promise 对象成功的结果存入数组
                    values[i] = value;
                    //每个 Promise 对象都成功，才算成功
                    if(count === promises.length){
                        resolve(values);
                    }
                },reason =>{
                    reject(reason);
                });
            }
        });
    }

    /**
     * Promise 函数对象 race 方法
     * @param promises 一组 Promise
     * @returns 返回一个 Promise，其结果由第一个完成的 Promise 决定
     */
    static race(promise){
        return new Promise((resolve,reject) => {
            for(let i = 0;i < promises.length;i++){
                Promise.resolve(promise[i]).then(value =>{
                    resolve(value);
                },reject => {
                    reject(reason);
                });
            }
        });
    }

}
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)

[4] [ 【Promise】自定义 - 手写Promise - Promise.all - Promise(executor)](https://blog.csdn.net/weixin_44972008/article/details/114134995)

[5] [ Web前端Promise教程从入门到精通](https://www.bilibili.com/video/BV1GA411x7z1)