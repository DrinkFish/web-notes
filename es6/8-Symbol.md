# Symbol

## 1.基本用法

ES6 引入了一种新的原始数据类型 Symbol ，表示独一无二的值，**最大的用法是用来定义对象的唯一属性名**。

ES6 数据类型除了 Number 、 String 、 Boolean 、 Object、 null 和 undefined ，`Symbol` 成为第七种数据类型 。

Symbol 是原始数据类型，不是对象，所以无法使用 new 命令。它可以接收一个字符串作为参数，表示 Symbol 示例的描述。

```js
let sy = Symbol("str");
console.log(sy);            // Symbol(str)
console.log(typeof(sy));    // "symbol"
console.log(sy.toString()); //"Symbol(str)"
```

Symbol 函数的参数只是表示当前 Symbol 值的描述，所以相同的 Symbol 函数返回值是不相等的。

```js
//无参
let sy1 = Symbol(); 
let sy2 = Symbol(); 
console.log(sy1 === sy2);// false

//相同参数
let sy3 = Symbol("str"); 
let sy4 = Symbol("str"); 
console.log(sy3 === sy4);// false
```

Symbol 不能和其他数据类型进行运算。

```js
console.log(s + "ss");  //TypeError: Cannot convert a Symbol value to a string
console.log(s > 10);    //TypeError: Cannot convert a Symbol value to a number
console.log(s + s);     //TypeError: Cannot convert a Symbol value to a number
```

## 2.作为属性名

由于每一个 Symbol 的值都是不相等的，所以 Symbol 作为对象的属性名，可以保证属性不重名。

```js
// 写法1
let syObj1 = {};
syObj1[sy] = "symbol";
console.log(syObj1);    //{Symbol(): "symbol"}
 
// 写法2
let syObj2 = {
  [sy]: "symbol"
};
console.log(syObj2);    //{Symbol(): "symbol"}
 
// 写法3
let syObj3 = {};
Object.defineProperty(syObj3, sy, {value: "symbol"});
console.log(syObj3);    //{Symbol(): "symbol"}
```

Symbol 作为对象属性名时，不能使用**“.”点运算符**，要用 [] 方括号。因为点运算符后面是字符串，所以取到的是字符串 sy 属性，而不是 Symbol 值 sy 属性。

```js
let sy = Symbol();
let obj = {};
obj[sy] = "lalala";

console.log(obj[sy]);   //lalala
console.log(obj.sy);    //undefined
```

## 3.定义常量

使用 Symbol 定义常量的最大好处，就是其他任何值都不可能有相同的值了。

```js
const log = {};

log.levels = {
  DEBUG: Symbol('debug'),
   INFO: Symbol('info'),
   WARN: Symbol('warn')
};
console.log(log.levels.DEBUG);//Symbol(debug)
console.log(log.levels.INFO); //Symbol(info)
```

## 4.Symbol.for() 和 Symbol.keyFor()

`Symbol.for() ` 方法可以实现同一个 Symbol值。它接收一个字符串作为参数，并搜索有没有以该参数作为名称的 Symbol 值，如果有，就返回 Symbol 值；没有就新建一个以该字符串为名称的 Symbol 值，并注册到全局。

```js
let s1 = Symbol.for('symbol');
let s2 = Symbol.for('symbol');

console.log(s1 === s2); // true
```

**与 Symbol（）的区别：**

- Symbol.for() 会被登记在全局环境中供搜索，而 Symbol() 不会。

- Symbol.for() 不会每次调用就返回一个新的 Symbol 类型的值，而是会先检查给定的 key 是否已经存在，如果不存在才会新建一个值。

  ```js
  let s1 = Symbol.for('symbol');
  let s2 = Symbol.for('symbol');
  let s3 = Symbol('symbol');
  let s4 = Symbol('symbol');
  
  console.log(s1 === s2); // true
  console.log(s3 === s4); // false
  console.log(s1 === s3); // false
  ```

  由于 Symbol() 没有登记机制，所以每次调用都会返回一个不同的值。

`Symbol.keyFor()`方法返回一个已经登记的 Symbol 类型的 key。

```js
let s1 = Symbol.for("symbol");
console.log(Symbol.keyFor(s1)); // "symbol"

let s2 = Symbol("symbol");
console.log(Symbol.keyFor(s2)); // undefined
```

由于 Symbol() 不会登记值，所以 s2 属于未登记的 Symbol，使用Symbol.keyFor() 无法获取登记值。

## 6.内置的Symbol

除了定义自己使用的 Symbol 值以外，ES6 还提供了 11 个内置的 Symbol 值。

| 属性                      | 描述                                                         |
| ------------------------- | ------------------------------------------------------------ |
| Symbol.hasInstance        | 用于判断某对象是否为某构造器的实例。当其他对象使用instanceof运算符，判断是否为该对象的实例时，会调用这个方法。 |
| Symbol.isConcatSpreadable | 布尔值，用于配置某对象作为Array.prototype.concat()方法的参数时是否展开其数组元素。 |
| Symbol.species            | 是个函数值属性，其被构造函数用以创建派生对象。               |
| Symbol.match              | 指定了匹配的是正则表达式而不是字符串。String.prototype.match() 方法会调用此函数。 |
| Symbol.replace            | 指定了当一个字符串替换所匹配字符串时所调用的方法。 String.prototype.replace() 方法会调用此方法。 |
| Symbol.search             | 指定了一个搜索方法，这个方法接受用户输入的正则表达式，返回该正则表达式在字符串中匹配到的下标。 |
| Symbol.split              | 指向 一个正则表达式的索引处分割字符串的方法。 这个方法通过 String.prototype.split() 调用。 |
| Symbol.iterator           | 为每一个对象定义了默认的迭代器。该迭代器可以被 for...of 循环使用。 |
| Symbol.toPrimitive        | 当一个对象转换为对应的原始值时，会调用此函数。               |
| Symbol. toStringTag       | 在该对象上面调用 toString 方法时，返回该方法的返回值。       |
| Symbol. unscopables       | 该对象指定了使用 with 关键字时，哪些属性会被 with环境排除。  |

1. **Symbol.hasInstance**

   ```js
    //判断[]是否为MyArray类型
   class MyArray{
     static [Symbol.hasInstance](param){
       return Array.isArray(param);
     }
   }
   console.log([] instanceof MyArray);//true
   ```

2. **Symbol.isConcatSpreadable**

   ```js
   //对于数组，默认行为是可以展开，Symbol.isConcatSpreadable默认等于undefined。该属性等于true时，也有展开的效果。
   let arr1 = ['a','b'];
   console.log(arr1[Symbol.isConcatSpreadable]);//undefined
   console.log(['a','d'].concat(arr1));//["a", "d", "a", "b"]
   
   arr1[Symbol.isConcatSpreadable] = false;
   console.log(['e'].concat(arr1));//["e",["a","b"]]
   
   //对于对象，默认不展开。它的Symbol.isConcatSpreadable属性设为true，才可以展开。
   let obj = {length: 2, 0: "11", 1: "22"};
   console.log(['aaa'].concat(obj));//["aaa",{"0": "11","1": "22","length": 2}]
   
   obj[Symbol.isConcatSpreadable] = true;
   console.log(["aaa"].concat(obj));//["aaa", "11", "22"]
   ```

3. **Symbol.species**

   ```js
   class MyArray extends Array {
     static get [Symbol.species]() {
       return Array;
     }
   }
   
   const a = new MyArray(1, 2, 3);
   const b = a.map(x => x);
   
   console.log(a instanceof MyArray); // true
   console.log(b instanceof MyArray); // false
   console.log(a instanceof Array); // true
   console.log(b instanceof Array); // true
   ```

4. **Symbol.match**

   ```js
   class MyMatcher {
     [Symbol.match](string) {
       return 'hello world'.indexOf(string);
     }
   }
   
   let index = 'wo'.match(new MyMatcher());
   console.log(index);//6
   ```

5. **Symbol.replace**

   ```js
   x[Symbol.replace] = (...s) => console.log(s);
   
   'Hello'.replace(x, 'World') // ["Hello", "World"]
   ```

6. **Symbol.search**

   ```js
   class MySearch {
     constructor(value) {
       this.value = value;
     }
     [Symbol.search](string) {
       return string.indexOf(this.value);
     }
   }
   let index = 'search'.search(new MySearch('c'));
   console.log(index);//4
   ```

7. **Symbol.split**

   ```js
   let exp =  {
       pat:'in',
       [Symbol.split](str) {
         return str.split(this.pat);
       }
   }
   
   console.log("dayinlove".split(exp));// ["day", "love"]
   ```

8. **Symbol.iterator**

   ```js
   let myIterable = {}
   myIterable[Symbol.iterator] = function* () {
       yield 1;
       yield 2;
       yield 3;
   };
   console.log([...myIterable]); // [1, 2, 3]
   ```

9. **Symbol.toPrimitive**

   ```js
   let obj = {
     [Symbol.toPrimitive](hint) {
       switch (hint) {
         case 'number':
           return 123;
         case 'string':
           return 'str';
         case 'default':
           return 'default';
         default:
           throw new Error();
        }
      }
   };
   
   console.log(2 * obj);  // 246
   console.log(3 + obj);  // "3default"
   console.log(`${obj}`); // "str"
   ```

10. **Symbol. toStringTag**

   ```js
   class ValidatorClass {
     get [Symbol.toStringTag]() {
       return "Validator";
     }
   }
   
   let s = Object.prototype.toString.call(new ValidatorClass());
   console.log(s); // "[object Validator]"
   ```

11. **Symbol. unscopables**

    ```js
    let obj = {
      foo: 1,
      bar: 2
    };
    
    obj[Symbol.unscopables] = {
      foo: false,
      bar: true
    };
    
    with(obj) {
      console.log(foo); // 1
      console.log(bar); // ReferenceError: bar is not defined
    }
    ```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)