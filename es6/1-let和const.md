# let 和 const

## 1. let 命令

`let `命令用来声明变量，它的用法类似 var，但是所有声明的变量，只在 let 命令所在的代码块内有效。有如下几个特点：

### 块级作用域

js 作用域包括：全局作用域和函数作用域，在 ES6 中新增了块级作用域，声明内容只在代码块 {} 有效。

```javascript
{
  let a = 1;
  var b = 2;
}
console.log(b);//2
console.log(a);//报错：ReferenceError: a is not defined
```

为什么需要块级作用域？

1、内层变量可能会覆盖外层变量。

2、用于循环时的变量泄露为全局变量。

```js
//情景一
function fn(){
    console.log(temp);
    if(false){
    		var temp = "es6";
    }
}
fn();//undefined

//情景二
var a = 10;
for(var i = 1; i < a; i++){
}
console.log(i);//10
for(let j = 1; j < a; j++){
}
console.log(j);//报错：ReferenceError: j is not defined
```

情景一中，执行 fn 方法，if 代码块外部使用变量 temp，由于 if 内使用 var 声明 temp 变量，导致 temp 变量提升，覆盖外层的变量，所以输出结果是 undefined 而不是 temp is not defined。

### 暂存性死区

通过 `var` 声明的变量，有初始值 undefined，但是如果区块中存在 let 和 const 声明变量，凡是在声明之前就使用了这些变量，就会导致 `ReferenceError`,称作“暂存死区”（temporal dead zone，简称 TDZ）。

```js
//TDZ开始
tmp = 'abc'; // ReferenceError
console.log(tmp); // ReferenceError

let tmp; // TDZ结束
console.log(tmp); // undefined

tmp = 123;
console.log(tmp); // 123
```

如果使用 let 声明，使用 `typeof `检测在暂存死区中的变量，会抛出`ReferenceError`异常。

```js
console.log(typeof undeclaredVariable);//undefined

console.log(typeof i);//报错：ReferenceError: Cannot access 'i' before initialization
let i = 10;
```

### 不允许重复声明

不能在相同作用域内，重复声明同一个变量。

```javascript
{
  let a = 1;
  let a = 2;
  //报错：SyntaxError: Identifier 'a' has already been declared
}
```

### 不存在变量提升

变量提升就是在变量创建之前使用，let 不存在，var 存在。

```javascript
console.log(a);//输出默认值：undefined
console.log(b);//报错：ReferenceError: Cannot access 'b' before initialization

var a = 'hello';
let b = 'ES6';
```

### 不影响作用域链

作用域链可理解为，代码块内有代码块，上级代码块中的局部变量在下级代码块中也可以使用。

```js
let a = 10;
function fn(){
  console.log(a);
}
fn();//10
```

## 2. const 命令

`const`声明一个只读的常量，一旦声明，它的值不可以改变。

### 声明必须赋初始值

```js
const PI;//报错：SyntaxError: Missing initializer in const declaration
const PI = 3.14;
```

### 不允许重复声明

```js
const PI = 3.14;
const PI = 3.14;//报错：SyntaxError: Identifier 'PI' has already been declared
```

### 值不允许修改

```js
//基本类型
const PI = 3.14;
PI = 3;//报错：TypeError: Assignment to constant variable.

//引用类型
let arr = [1,2,3,4,5];
arr[2] = 5;
console.log(arr);// [1, 2, 5, 4, 5]
```

- **注意**：使用 const 声明时，对于基本类型不能改变它的值，对于引用类型如对象或数组，变量指向的是内存地址，const 只能保证这个引用地址是固定的，但是可以改变引用的内容。

### 块级作用域

```js
{
    const PI = 3.14;
    console.log(PI);//3.14
}
console.log(PI);//报错：ReferenceError: PI is not defined
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)



