# 函数的扩展

## 1.函数参数的扩展

### 默认参数

只有在未传递参数，或者参数为 `undefined` 时，默认参数才会生效。`null` 被认为是有效的值传递。

```js
function fn(a,b = 1){
    console.log(a +","+ b);
}
fn(123,123);//123,123
fn(123);    //123,1
fn();       //undefined,1
fn(123,null);//123,null
fn(123,undefined);//123,1
```

当使用函数默认参数时，不允许有同名参数，否则会报错。

```js
//SyntaxError: Duplicate parameter name not allowed in this context
function fn(a,b,b = 1){
    console.log(a +","+ b);
}
```

函数参数默认值存在暂时性死区，在函数参数默认值表达式中，还未初始化赋值的参数值无法作为其他参数的默认值。

```js
function fn(a, b = a){
    console.log(a +","+ b);
}
fn(1);  //1,1
 
function f(a = b){
    console.log(a);
}
f();  //ReferenceError: b is not defined
```

当指定了默认值后，函数的 length 属性将返回**没有指定默认值的参数个数**。如果设置的默认值参数不是尾部参数，那么它后面的参数也不计入 length 属性。

```js
//没有默认值
console.log(
    (function (a) {}).length
);//1

//设置默认值
console.log(
    (function (a = 1) {}).length
);//0

//默认值不在尾部
console.log(
    (function (a, b = 1, c) {}).length
);//1

//默认值在尾部
console.log(
    (function (a, b, c = 1) {}).length
);//2
```

参数的默认值还可以和解构赋值一起使用。

```js
//函数参数的默认值是空对象，设置对象解构赋值的默认值
function fn1({a = 0, b = 0} = {}) {
  console.log([a, b]);;
}

//函数参数的默认值是一个有具体属性的对象，不设置对象解构赋值的默认值
function fn2({a, b} = { a: 0, b: 0 }) {
  console.log([a, b]);
}

//函数调用
// 函数没有参数的情况
fn1(); // [0, 0]
fn2(); // [0, 0]

// a 和 b 都有值的情况
fn1({a: 1, b: 2}); // [1, 2]
fn2({a: 1, b: 2}); // [1, 2]

// a 有值，b 无值的情况
fn1({a: 1}); // [1, 0]
fn2({a: 1}); // [1, undefined]

// a 和 b 都无值的情况
fn1({}); // [0, 0];
fn2({}); // [undefined, undefined]
```

###  rest 参数

不定参数(rest)用来表示不确定参数个数，格式为 `...变量名`，由...加上一个具名参数标识符组成。

```js
function add(...args){
    let sum = 0;
    for(let val of args){
        sum += val;
    }
    console.log(sum);
}

add();          //0
add(1, 2);      //3
add(1, 2, 3, 4);//10
```

**具名参数只能放在参数组的最后，并且有且只有一个不定参数。**

```js
//SyntaxError: Rest parameter must be last formal parameter
function add(...args, a){

}
```

## 2.箭头函数

ES6允许使用箭头（=>）定义函数，箭头函数提供了一种更加简洁的函数书写方式，箭头函数多用于匿
名函数的定义。

```js
参数 => 函数体

var f = v => console.log(v);
f(1);   //1

//等同于
let fn = function(v){
    console.log(v);
}
fn(1);  //1
```

当箭头函数不需要参数或者需要多个参数时，就使用一个圆括号代表参数部分。

```js
var f = () => console.log(阿巴阿巴);
// 等同于
var f = function () { console.log(阿巴阿巴); };

var sum = (a, b) => a + b;
// 等同于
var sum = function(a, b) {
  return a + b;
};
```

当箭头函数的代码块部分多于一条语句，就需要使用大括号 {} 将他们包裹起来。

```js
let sum = (a, b) => {return a + b};

console.log(sum(11,22));//33
```

当箭头函数要返回对象的时候，为了区分于代码块，要用 **()** 将对象包裹起来。

```js
// 不报错
var fn = (id,name) => ({id: id, name: name});
fn(6,"Winkyu");  //{id: 6, name: "Winkyu"}

// 报错
var f = (id,name) => {id: id, name: name};
f(6,2);  //SyntaxError: Unexpected token ':'
```

### 注意事项

1. **箭头函数中的 `this `是指向声明时所在作用域下 this 的值。**
2. 箭头函数不可以当做构造函数，也就是不能对箭头函数使用 new 命令。
3. 不可以使用 arguments 对象，可以使用 rest 参数代替。

```js
//1. this 是静态的. this 始终指向函数声明时所在作用域下的 this 的值
function getName(){
    console.log(this.name);
}
let getName1 = () => {
    console.log(this.name);
}

//设置 window 对象的 name 属性
window.name = '阿巴阿巴';
const person = {
    name: "Winkyu"
}

//直接调用
getName();  //阿巴阿巴
getName1(); //阿巴阿巴

//call 方法调用
getName.call(person);   //Winkyu
getName1.call(person);  //阿巴阿巴

//2. 不能作为构造实例化对象
let Man = (name, age) => {
  	this.name = name;
  	this.age = age;
}
let me = new Man('Winkyu',18);
console.log(me);//TypeError: Man is not a constructor

//3. 不能使用 arguments 变量
let fn = () => {
  console.log(arguments);
}
fn();//ReferenceError: arguments is not defined
```

### 适合使用场景

当需要维护一个 this 的上下文。

```js
var Person = {
    'age': 18,
    'sayHello': function () {
      setTimeout(()=>{
        console.log(this.age);
      });
    }
};
var age = 20;
Person.sayHello();  // 18

```

### 不适合使用场景

- 不适合定义对象的方法。
- 不适合定义结合动态上下文的回调函数（事件绑定函数）。

```js
var Person = {
    'age': 18,
    'sayHello': ()=>{
        console.log(this.age);
      }
};
var age = 20;
Person.sayHello();  // 20
// 此时 this 指向的是全局对象

document.getElementById('btn').addEventListener('click', () => {
    this.classList.toggle('on');
});
//监听函数是箭头函数，所以监听函数里面的 this 指向的是定义的时候外层的 this 对象，即 Window。
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)
