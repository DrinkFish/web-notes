# 对象的扩展

## 1.属性的简洁表示法

ES6 中允许在大括号里面，直接写入变量和函数，作为对象的属性和方法。

```js
const NAME = "Winkyu";
const AGE  = 18;
const PERSON = {NAME, AGE};
console.log(PERSON);//{NAME: "Winkyu", AGE: 18}
//等同于
const person = {name: NAME, age: AGE}
console.log(person);//{name: "Winkyu", age: 18}
```

方法也可以简写。

```js
const person1 = {
  方法：
    eat(){
        console.log("吃吃吃");
    }
}
person1.eat();//吃吃吃
//等同于
const person2 = {
    eat:function(){
        console.log("吃吃吃");
    }
}
person2.eat();//吃吃吃
```

## 2.属性名表达式

ES6 允许用表达式作为属性名，但一定要将表达式放在括号内。

```js
let obj = {
    id: 1,
    ['my'+'name']: 'Winkyu'
}
console.log(obj);//{id: 1, myname: "Winkyu"}
```

表达式还可以用于定义方法名。

```js
let obj = {
  ['h' + 'ello']() {
    return 'hi';
  }
};
console.log(obj.hello()); //“hi”
```

注意：属性名表达式与属性的简洁表示法，不能同时使用，否则会报错。

```js
const hello = "Hello";
//正确写法
const obj1 = {
    [hello+"1"]:"world"
};
console.log(obj1);//{Hello1: "world"}
//错误写法
const obj2 = {
    [hello]
};
console.log(obj2);//SyntaxError: Unexpected token '}'
```

## 3.对象的扩展运算符

扩展运算符（`...`），用于取出参数对象所有可遍历属性，然后拷贝到当前对象。

```js
let person = {name: "Winkyu", age: 18};
let one = { ...person };
console.log(one);//{name: "Winkyu", age: 18}
```

用于合并两个对象。

```js
let one = {name: "Winkyu", age: 18};
let two = {gender: 'man'};
let person = {...one, ...two};
console.log(person);//{name: "Winkyu", age: 18, gender: "man"}
```

当自定义属性和拓展运算符对象的属性相同时：

- 在后面的会覆盖在前面的。

  ```js
  let one = {name: "Winkyu", age: 18};
  let person1 = {...one, name: "lulu"};
  console.log(person1);//{name: "lulu", age: 18}
  
  let person2 = {name: "lulu", ...one};
  console.log(person2);//{name: "Winkyu", age: 18}
  ```

- 若扩展运算符后面是空对象 { } 或者是 null ，undefined，

  ```js
  let a = {...{}, a: 1, b: 2};
  console.log(a);;  //{a: 1, b: 2}
  
  let b = {...null, ...undefined, a: 1, b: 2};
  console.log(b);;  //{a: 1, b: 2}
  ```


## 4.对象的新增方法

- `Object.is()`：比较两个值是否严格相等，与`===` 的行为基本一致。与它不同的是： +0 不等于 -0，NaN 等于自身。

  ```js
  console.log(Object.is(1,1));        // true
  console.log(Object.is("es6","es6"));// true
  console.log(Object.is([1,2,3],[1,2,3]));// false
  console.log(Object.is({name:"Winkyu"},{name:"Winkyu"}));// false
  
  console.log(Object.is(+0,-0));  //false
  console.log(+0 === -0);         //true
  console.log(Object.is(NaN,NaN));//true
  console.log(NaN === NaN);       //false
  ```

- `Object.assign()`：用于对象的合并。该方法第一个参数是目标对象，后面的都是源对象。如果目标对象与源对象有同名属性，则后面的属性会覆盖前面的属性。

  ```js
  let target  = { a: 1, b: 11 };
  let source1 = { b: 2 };
  let source2 = { c: 3 };
  Object.assign(target, source1, source2);
  console.log(target);//{a: 1, b: 2, c: 3}
  ```

  若该函数只有一个参数，当参数为对象时，直接返回该对象；当参数不是对象时，会先将参数转为对象然后返回。

  ```js
  console.log(Object.assign(3));;         // Number {3}
  console.log(typeof Object.assign(3));;  // "object"
  ```

  由于`undefined`和`null`无法转成对象，所以如果它们作为参数，就会报错。

  ```js
  //报错：TypeError: Cannot convert undefined or null to object
  console.log(Object.assign(undefined));
  console.log(Object.assign(null));
  ```

  **注意事项：**
  
- 该方法的拷贝方式为**浅拷贝**，如果源对象某个属性的值是对象，那么目标对象拷贝得到的是这个对象的引用。

  ```js
  let sourceObj = {a: { b: 1}};
  let targetObj = {c: 3};
  Object.assign(targetObj, sourceObj);
  //赋值
  targetObj.a.b = 2;
  console.log(sourceObj.a.b);//2
  ```

- **同名属性替换**，会将目标对象的同名属性整个替换掉。

  ```js
  let targetObj = {a: {b: 2, c: 2}};
  let sourceObj = {a: {b: 1}};
  Object.assign(targetObj, sourceObj);
  console.log(targetObj);//{a: {b: 1}}
  //将 targetObj 的 a 属性整个替换，而不是 {a: {b: 1,c: 2}}
  ```

- `Object.keys()`：返回一个数组，成员是对象自身的所有可遍历属性的**键名**。

- `Object.values()`：返回一个数组，成员是对象自身的所有可遍历属性的**键值**。

- `Object.values()`：返回一个数组，成员是对象自身的所有可遍历属性的**键值对**数组。

  ```js
  let obj = { name: 'Winkyu', age: 18 };
  console.log(Object.keys(obj));      //["name", "age"]
  console.log(Object.values(obj));    //["Winkyu", 18]
  console.log(Object.entries(obj));   //[["name", "Winkyu"],["age", 18]]
  ```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)