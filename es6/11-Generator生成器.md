# Generator 生成器

`Generator` 函数是 ES6 提供的一种异步编程解决方案。

## 1.Generator 函数组成

Generator 有两个区分于普通函数的部分：

-  function 后面，函数名之前有个 * ；
- 函数内部有 yield 表达式。

```js
//定义Generator 函数 fn()
function* fn(){
    console.log(111);
    yield 1;
    console.log(222);
    yield 2;
    return "ok";
}
let iterator = fn();
//执行机制
console.log(iterator.next());
//111

console.log(iterator.next());
//{value: 1, done: false}
//222

console.log(iterator.next());
//{value: 2, done: false}
//{value: "ok", done: true}

console.log(iterator.next());
//{value: undefined, done: true}
```

**执行机制**

调用 Generator 函数时，它不会向普通函数一样执行，而是返回一个指向内部状态对象的指针，需要调用 Iterator 的 next() 方法。上述执行过程为：

第一次调用 next() 方法，从 Generator 函数头部开始执行，执行打印语句，接着**遇到 yield 就停止，并将 yield 后边表达式 1 作为返回对象的 value 值**，此时函数还没执行完，返回对象的 done 属性值是 false。

第二次调用 next() 方法，同上步。

第三次调用 next() 方法，执行函数中 return 语句，意味着函数执行完成，将 return 后表达式作为返回对象的 value 值，返回对象的 done 属性值是 true。

第四次调用 next() 方法，此时函数已经执行完成，所以返回 value 属性值为 undefined，done 属性值是 true。如果执行第三步时，没有 return 语句的话，就直接返回 {value: undefined, done: true}。

**yield 表达式**

`yield `表达式就是暂停标志，遇到就会暂停后面的操作，并将紧跟在 yield 表达式后面的那个表达式的值，作为返回的对象的 value 属性值。

**与 return 的区别**：

- 相同点：都可以返回紧跟在表达式后面的那个表达式的值。
- 不同点：yield 只会让函数暂停执行，下一次再从该位置继续向后执行，而 return 语句不具备位置记忆的功能。

## 2.next() 方法的参数

next() 方法可以带一个参数，该参数会作为 yield 表达式的返回值。

```js
function* fn(a){
    let b = 1 + (yield (a * 2));
    let c = yield (b / 2);
    return (a + b + c);
}
```

**若调用 next() 不传入参数：**

```js
let it1 = fn(2);
console.log(it1.next());//{value: 4, done: false}
console.log(it1.next());//{value: NaN, done: false}
console.log(it1.next());//{value: NaN, done: true}
```

上述代码，第一次运行，a = 2，传入后执行，返回对象的 value 值是 4，done 属性值为 false。第二次执行 next() 方法，由于未传入参数，b的值是 1 + undefined，即 NaN。b / 2 还是 NaN，第三次执行未传入参数， c 的值还是 undefined，最后返回对象的属性为 2 + NaN + undefined，为 NaN。

**若带参调用 next()：**

```js
let it2 = fn(2);
console.log(it2.next());//{value: 4, done: false}
console.log(it2.next(4));//{value: 2, done: false}
console.log(it2.next(3));//{value: 9, done: true}
```

第一次执行 next()，`a = 2`，a * 2 = 4 作为第一个 field 表达式的返回值，执行暂停；第二次执行，将第上一次的 yield 表达式的值设置为 4，所以 `b = 5`，接着执行 b / 2 = 2.5，作为第二个 yeild 表达式的返回值，执行暂停；第三次执行，将参数 3 设置为上一次 yield 表达式的值，也就是` c = 3`，执行 return (a + b + c) ，即 2 + 5 +3 = 10，执行结束。 

## 3.使用 for...of 遍历 Generator

使用 for...of 循环也可以遍历 Generator 函数运行时生成的 Iterator 对象，此时不再需要 next()。

```js
function * gen(){
    console.log(111);
    yield '一只没有耳朵';
    console.log(222);
    yield '一只没有尾部';
    console.log(333);
    yield '真奇怪';
    return 444;
}

for(let v of gen()){
    console.log(v);
}
// 111
// 一只没有耳朵
// 222
// 一只没有尾部
// 333
// 真奇怪
```

上述示例中注意，当 next() 方法的返回对象的 done 属性为 true，for...of 循环就会终止，且不包含该返回对象，所以最后一句 return 语句不在 for...of 循环中。

## 4.throw() 和 return()

- `throw()` 方法可以再 Generator 函数体外面抛出异常，再函数体内部捕获。
- `return()` 方法返回给定值，并结束遍历 Generator 函数。提供参数时，返回该参数；不提供参数时，返回 undefined 。

```js
function* gen() {
    try {
        yield 1;
    } catch (e) {
        console.log('内部捕获', e);
    }
};
let fn = gen();
console.log(fn.next());//{value: 1, done: false}

try {
    fn.throw('a');//"内部捕获 a"
    fn.throw('b');//"外部捕获 b"
} catch (e) {
    console.log('外部捕获', e);
}
```

上述抛出两个错误，第一个被函数 gen 内部的 catch 语句捕获，由于 Generator 函数体内的 catch 已经执行过，就不会再捕获错误了，第二个被外部的 catch 语句捕获。

```js
function * gen(){
    yield '一只没有耳朵';
    yield '一只没有尾部';
    yield '真奇怪';
}

let fn = gen();
console.log(fn.next());//{value: "一只没有耳朵", done: false}
console.log(fn.return("先溜啦"));//{value: "先溜啦", done: true}
console.log(fn.next());//{value: undefined, done: true}
```

上述可以看出调用 return() 时，返回对象的 done 属性值为 true，再执行时，value 属性值为 undefined。

## 5.yield* 表达式

`yield* `表达式表示 yield 返回一个遍历器对象，用于在 Generator 函数内部，调用另一个 Generator 函数。

```js
function* foo() {
  yield 'a';
  yield 'b';
}

function* bar() {
  yield 'x';
  yield* foo();
  yield 'y';
}

// 等同于
function* bar() {
  yield 'x';
  yield 'a';
  yield 'b';
  yield 'y';
}

for (let v of bar()){
  console.log(v);
}
//x
//a
//b
//y
```

## 6.使用场景

- **实现 Iterator**

  为不具备 Iterator 接口的对象提供了遍历操作。

```js
function* fn(obj) {
    let keys = Object.keys(obj);
    for (const key of keys) {
        yield [key,obj[key]];
    }
}

let myObj = { foo: 123, bar: "iterator" };
for (let [key, value] of fn(myObj)) {
  console.log(`${key}:${value}`);
}
//foo:123
//bar:iterator
```

- **异步操作同步化表达**

通过 Generator 函数部署 ajax 可以用同步的方式表达。

```js
//模拟获取  用户数据  订单数据  商品数据 
function getUsers(){
  setTimeout(()=>{
    iterator.next("用户数据");
  }, 1000);
}

function getOrders(){
  setTimeout(()=>{
    iterator.next("订单数据");
  }, 1000)
}

function getGoods(){
  setTimeout(()=>{
    iterator.next("商品数据");
  }, 1000)
}

function * gen(){
  let users  = yield getUsers();
  console.log(users);
  let orders = yield getOrders();
  console.log(orders);
  let goods  = yield getGoods();
  console.log(goods);
}

//调用生成器函数
let iterator = gen();
iterator.next();
```

# 📚 Reference

[1] [ 菜鸟教程-ES6 教程](https://www.runoob.com/w3cnote/es6-tutorial.html)

[2] [ 《ECMAScript 6 入门教程》](https://es6.ruanyifeng.com/)

[3] [ 尚硅谷Web前端ES6教程，涵盖ES6-ES11](https://www.bilibili.com/video/BV1uK411H7on)